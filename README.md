# Welcome to TASC Application

> This is an application made by Group 5 for the Group Assignment of Advanced Programming course. The members of our group and their responsibilities for Group Assignment 3 are:
> 
> 1.  Inez Zahra Nabila - 1806241091 was responsible for enabling the uses cases for sending notifications and reminders to the appropriate users.
> 2.  Munirudeen Maricar - 1806188193 was responsible for creating the README file, refactoring the code, enabling the use case for creating and accepting/rejecting the appointment, creating the HTML file and updating the test cases, implementing the database using PostgreSQL and deploying the web application to Heroku.
> 3.  Muhammad Ivan Taftazani - 1806241122. 

---

The badges we have implemented to get an overview of our project are [![Pipeline](https://gitlab.cs.ui.ac.id/software-engineering-2020-5/tasc/badges/master/pipeline.svg)](https://gitlab.cs.ui.ac.id/software-engineering-2020-5/tasc) and [![Coverage](https://gitlab.cs.ui.ac.id/software-engineering-2020-5/tasc/badges/master/coverage.svg)](https://gitlab.cs.ui.ac.id/software-engineering-2020-5/tasc).
The link to our web application is [tasc-application.herokuapp.com](https://tasc-application.herokuapp.com/).
