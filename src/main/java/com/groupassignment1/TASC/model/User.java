package com.groupassignment1.TASC.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "User")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotNull
    private String name;

    @NotNull
    private String email;

    @NotNull
    private String role;

    @ElementCollection(targetClass = Course.class)
    private List<Course> coursesTaken;

    public User() {
    }

    public User(String name, String email, String role) {
        this.name = name;
        this.email = email;
        this.role = role;
    }

    public User(String name, String email, String role, List<Course> coursesTaken) {
        this.name = name;
        this.email = email;
        this.role = role;
        this.coursesTaken = coursesTaken;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<Course> getCoursesTaken() {
        return coursesTaken;
    }

    public void setCoursesTaken(List<Course> coursesTaken) {
        this.coursesTaken = coursesTaken;
    }

    public Course createCourse(String courseName) throws Exception {
        if (this.role.equalsIgnoreCase("Admin")) {
            return new Course(courseName);

        }
        throw new Exception("Not authorized.");
    }

    public void enrollCourse(Course course) throws Exception {
        if (!this.role.equalsIgnoreCase("Student")) {
            throw new Exception("Not authorized.");
        }
        this.coursesTaken.add(course);
    }

    public void makeAppointment(Appointment appointment, Course course) throws Exception {
        if (!this.role.equalsIgnoreCase("Teaching Assistant")) {
            throw new Exception("Not authorized.");
        }
        if (!course.getCalendar().getAppointments().contains(appointment)){
            course.getCalendar().getAppointments().add(appointment);
        }
    }

    public void acceptAppointment(Appointment appointment) throws Exception {
        if (!this.role.equalsIgnoreCase("Teaching Assistant") && !this.role.equalsIgnoreCase("Student")) {
            throw new Exception("Not authorized.");
        }
        if (appointment.getStudent().getName().equals(this.name)) {
            appointment.setStatus(true);
        }
    }
}
