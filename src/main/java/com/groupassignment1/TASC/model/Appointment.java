package com.groupassignment1.TASC.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "appointment")
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int appointmentId;

    @NotNull
    @ManyToOne(cascade = CascadeType.ALL)
    private User student;

    @NotNull
    @ManyToOne(cascade = CascadeType.ALL)
    private User teachingAssistant;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @NotNull
    private boolean status;

    @NotNull
    private String message;

    @NotNull
    private boolean notifier;

    public Appointment() {
    }

    public Appointment(User student, User teachingAssistant, Date date, String message) {
        this.student = student;
        this.teachingAssistant = teachingAssistant;
        this.date = date;
        this.status = false;
        this.message = message;
        this.notifier = false;
    }

    public int getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(int appointmentId) {
        this.appointmentId = appointmentId;
    }

    public User getStudent() {
        return student;
    }

    public void setStudent(User student) {
        this.student = student;
    }

    public User getTeachingAssistant() {
        return teachingAssistant;
    }

    public void setTeachingAssistant(User teachingAssistant) {
        this.teachingAssistant = teachingAssistant;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getNotifier() {
        return notifier;
    }

    public void setNotifier(boolean notifier) {
        this.notifier = notifier;
    }
}