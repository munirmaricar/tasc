package com.groupassignment1.TASC.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Course")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int courseId;

    @NotNull
    private String courseName;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    private Calendar calendar;

    public Course() {
    }

    public Course(String courseName) {
        this.courseName = courseName;
        this.calendar = new Calendar();
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }
}