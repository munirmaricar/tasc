package com.groupassignment1.TASC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TASCApplication {
	public static void main(String[] args) {
		SpringApplication.run(TASCApplication.class, args);
	}
}
