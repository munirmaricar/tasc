package com.groupassignment1.TASC.controller;

import com.groupassignment1.TASC.model.*;
import com.groupassignment1.TASC.service.MailerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;


@RestController
public class AppointmentController {
    protected AppointmentRepository appointmentRepository;
    protected UserRepository userRepository;
    protected CalendarRepository calendarRepository;
    protected CourseRepository courseRepository;
    private MailerService sendingMail;
    SimpleDateFormat datetimeFormatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    @Autowired
    public AppointmentController(AppointmentRepository appointmentRepository, UserRepository userRepository,
                                 CalendarRepository calendarRepository, CourseRepository courseRepository, MailerService sendingMail) {
        this.appointmentRepository = appointmentRepository;
        this.userRepository = userRepository;
        this.calendarRepository = calendarRepository;
        this.courseRepository = courseRepository;
        this.sendingMail = sendingMail;
    }

    @PostMapping("/appointment")
    public Appointment postMakeAppointment(@Valid @RequestBody Map<String, String> body) throws ParseException, MessagingException {
        User student = userRepository.findOneByEmail(body.get("studentEmail"));
        User assistant = userRepository.findOneByEmail(body.get("assistantEmail"));
        Date date = datetimeFormatter.parse(body.get("date"));
        Appointment appointment = new Appointment(student, assistant, date, body.get("message"));
        Calendar calendar = courseRepository.findOneByCourseName(body.get("courseName")).getCalendar();
        calendar.getAppointments().add(appointment);
        appointmentRepository.save(appointment);
        calendarRepository.save(calendar);

        String content = "Hello! You have registered for an appointment with " + assistant.getName() + " on " + appointment.getDate() + ".";
        sendingMail.sendEmail(student.getEmail(), "New Appointment", content);

        content = "Hello! You have registered for an appointment with " + student.getName() + " on " + appointment.getDate() + ".";
        sendingMail.sendEmail(assistant.getEmail(), "New Appointment", content);

        return appointmentRepository.save(appointment);
    }

    @GetMapping("/appointment/status")
    public Appointment getAcceptAppointment(@Valid @RequestParam("appointmentID") int appointmentId) {
        Appointment appointment = appointmentRepository.findOneByAppointmentId(appointmentId);
        appointment.setStatus(true);
        return appointmentRepository.save(appointment);
    }

    @Scheduled(cron = "*/10 * * * *")
    public void run() throws MessagingException {
        List<Appointment> appointments = appointmentRepository.findAll();
        for (Appointment appointment : appointments) {
            Date date = java.util.Calendar.getInstance().getTime();
            if (appointment.getDate().getTime() - date.getTime() <= 3600) {
                String content = "Content";
                sendingMail.sendEmail(appointment.getStudent().getEmail(), content, "Appointment Reminder");
                sendingMail.sendEmail(appointment.getTeachingAssistant().getEmail(), content, "Appointment Reminder");
                appointment.setNotifier(true);
                appointmentRepository.save(appointment);
            }
        }
    }

}
