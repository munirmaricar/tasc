//package com.groupassignment1.TASC.config;
//
//import java.net.URI;
//import java.net.URISyntaxException;
//import javax.sql.DataSource;
//import org.apache.commons.dbcp2.BasicDataSource;
//
//import org.springframework.boot.autoconfigure.domain.EntityScan;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//
//@Configuration
//@ComponentScan
//@EntityScan("com.groupassignment1.TASC")
//@EnableJpaRepositories("com.groupassignment1.TASC")
//@PropertySource("classpath:db-config.properties")
//public class DBConfiguration {
//
//    @Bean
//    public DataSource dataSource() throws URISyntaxException {
//        URI dbUri = new URI(System.getenv("DATABASE_URL"));
//        String username = dbUri.getUserInfo().split(":")[0];
//        String password = dbUri.getUserInfo().split(":")[1];
//        String dbUrl = "jdbc:postgresql://"
//                + dbUri.getHost() + ':'
//                + dbUri.getPort() + dbUri.getPath();
//        BasicDataSource basicDataSource = new BasicDataSource();
//        basicDataSource.setUrl(dbUrl);
//        basicDataSource.setUsername(username);
//        basicDataSource.setPassword(password);
//        return basicDataSource;
//    }
//}
//
