package com.groupassignment1.TASC.controller;

import com.groupassignment1.TASC.service.MailerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.mail.MessagingException;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class MailerServiceTest {
    private MailerService mailerService;

    @BeforeEach
    void setUp() {
        mailerService = new MailerService();
    }

    @Test
    void testSendEmail() throws MessagingException {
        mailerService.sendEmail("maricar.munirudeen@gmail.com", "Test", "This is a test.");
        assertThat(mailerService).isNotNull();
    }
}


