package com.groupassignment1.TASC;

import com.groupassignment1.TASC.controller.AppointmentController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class TASCApplicationTests {

	@Autowired
	private AppointmentController appointmentController;

	@Test
	void contextLoads() {
		TASCApplication.main(new String[]{});
		assertThat(appointmentController).isNotNull();
	}
}
