package com.groupassignment1.TASC.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CalendarTest {
    private Calendar calendar;
    private User student;
    private User teachingAssistant;
    private Date date;
    private ArrayList<Appointment> listOfAppointments = new ArrayList<>();

    @BeforeEach
    void setUp() {
        calendar = new Calendar();
        student = new User("Test Student", "student@email.com", "Student", new ArrayList<Course>());
        teachingAssistant = new User("Test Teaching Assistant", "teachingassistant@email.com", "Teaching Assistant");
        date = new Date(2020, 7, 18, 00, 0, 0);
        listOfAppointments.add(new Appointment(student, teachingAssistant, date, "Review Design Patterns"));
    }

    @Test
    void testCalendarIDSettersandGetters() {
        int id = 1806188193;
        calendar.setCalendarId(id);
        assertThat(calendar.getCalendarId() == id).isTrue();
    }

    @Test
    void testListOfAppointmentsSettersandGetters() {
        calendar.setAppointments(listOfAppointments);
        assertThat(calendar.getAppointments().size() == 1).isTrue();
    }

}

