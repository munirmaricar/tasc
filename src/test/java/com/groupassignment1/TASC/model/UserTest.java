package com.groupassignment1.TASC.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class UserTest {
    private User admin;
    private User student;
    private User teachingAssistant;
    private Course apCourse;
    private Date date;
    private Appointment appointment;

    @BeforeEach
    void setUp() {
        admin = new User("Test Admin", "admin@email.com", "Student");
        student = new User("Test Student", "student@email.com", "Student", new ArrayList<Course>());
        teachingAssistant = new User("Test Teaching Assistant", "teachingassistant@email.com", "Teaching Assistant");
        apCourse = new Course("Advanced Programming");
        date = new Date(2020, 7, 18, 00, 0, 0);
        appointment = new Appointment(student, teachingAssistant, date, "Review Design Patterns");
    }

    @Test
    void testIDSetterAndGetter() {
        int id = 1806188193;
        student.setId(id);
        assertThat(student.getId() == id).isTrue();
    }

    @Test
    void testNameSetterAndGetter() {
        String name = "Test Test";
        admin.setName(name);
        assertThat(admin.getName().equals(name)).isTrue();
    }


    @Test
    void testEmailSetterAndGetter(){
        String email = "maricar.munirudeen@gmail.com";
        teachingAssistant.setEmail(email);
        assertThat(teachingAssistant.getEmail().equals(email)).isTrue();
    }

    @Test
    void testRoleSetterAndGetter(){
        String role = "Test Role";
        admin.setRole(role);
        assertThat(admin.getRole().equals(role)).isTrue();
    }

    @Test
    void testCoursesTakenSetterAndGetter(){
        ArrayList<Course> coursesTaken = new ArrayList<>();
        coursesTaken.add(apCourse);
        admin.setCoursesTaken(coursesTaken);
        assertThat(admin.getCoursesTaken().equals(coursesTaken)).isTrue();
    }

    @Test
    void testCreateCourseIfAdmin() throws Exception {
        try {
            Course daaCourse = student.createCourse("Design and Analysis of Algorithms");
            assertThat(daaCourse).isNotNull();
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Not authorized.")).isTrue();
        }

    }

    @Test
    void testCreateCourseIfNotAdmin() {
        try {
            Course daaCourse = student.createCourse("Design and Analysis of Algorithms");
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Not authorized.")).isTrue();
        }
    }

    @Test
    void testEnrollCourseIfStudent() throws Exception {
        student.enrollCourse(apCourse);
        assertThat(student.getCoursesTaken().contains(apCourse)).isTrue();
    }

    @Test
    void testEnrollCourseIfNotStudent() {
        try {
            teachingAssistant.enrollCourse(apCourse);
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Not authorized.")).isTrue();
        }
    }

    @Test
    void testMakeAppointmentIfNotTA(){
        try {
            admin.makeAppointment(appointment, apCourse);
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Not authorized.")).isTrue();
        }
    }

    @Test
    void testMakeAppointmentIfTA() throws Exception {
        teachingAssistant.makeAppointment(appointment, apCourse);
        assertThat(apCourse.getCalendar().getAppointments().contains(appointment)).isTrue();
    }

    @Test
    void testAcceptAppointmentIfNotStudent(){
        try {
            teachingAssistant.makeAppointment(appointment, apCourse);
            admin.acceptAppointment(appointment);
        } catch (Exception e) {
            assertThat(e.getMessage().equals("Not authorized.")).isTrue();
        }
    }

    @Test
    void testAcceptAppointmentIfStudent() throws Exception {
        teachingAssistant.makeAppointment(appointment, apCourse);
        student.acceptAppointment(appointment);
        assertThat(apCourse.getCalendar().getAppointments().get(0).getStatus()).isTrue();
    }
}

