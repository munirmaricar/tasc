package com.groupassignment1.TASC.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class AppointmentTest {
    private User student;
    private User teachingAssistant;
    private Course apCourse;
    private Date date;
    private Appointment appointment;

    @BeforeEach
    void setUp() {
        student = new User("Test Student", "student@email.com", "Student", new ArrayList<Course>());
        teachingAssistant = new User("Test Teaching Assistant", "teachingassistant@email.com", "Teaching Assistant");
        apCourse = new Course("Advanced Programming");
        date = new Date(2020, 7, 18, 00, 0, 0);
        appointment = new Appointment(student, teachingAssistant, date, "Review Design Patterns");
    }

    @Test
    void testAppointmentIDSetterAndGetter() {
        int id = 1806188193;
        appointment.setAppointmentId(id);
        assertThat(appointment.getAppointmentId() == id).isTrue();
    }

    @Test
    void testStudentSetterAndGetter() {
        User tempStudent = new User("Temporary Student", "temporarystudent@email.com", "Student", new ArrayList<Course>());
        appointment.setStudent(tempStudent);
        assertThat(appointment.getStudent().equals(tempStudent)).isTrue();
    }

    @Test
    void testTeachingAssistantSetterAndGetter() {
        User tempTA = new User("Temporary TA", "temporaryta@email.com", "Teaching Assistant");
        appointment.setTeachingAssistant(tempTA);
        assertThat(appointment.getTeachingAssistant().equals(tempTA)).isTrue();
    }

    @Test
    void testDateSetterAndGetter() {
        Date tempDate = new Date(2020, 8, 8, 00, 0, 0);
        appointment.setDate(tempDate);
        assertThat(appointment.getDate().equals(tempDate)).isTrue();
    }

    @Test
    void testStatusSetterAndGetter() {
        appointment.setStatus(true);
        assertThat(appointment.getStatus()).isTrue();
    }

    @Test
    void testMessageSetterAndGetter() {
        String tempMessage = "This is the test message for an appointment.";
        appointment.setMessage(tempMessage);
        assertThat(appointment.getMessage().equals(tempMessage)).isTrue();
    }

    @Test
    void testNotifierSetterAndGetter() {
        appointment.setNotifier(true);
        assertThat(appointment.getNotifier()).isTrue();
    }
}


