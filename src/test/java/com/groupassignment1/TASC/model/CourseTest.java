package com.groupassignment1.TASC.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CourseTest {
    private Course course;

    @BeforeEach
    void setUp() {
        course = new Course("Test Course");
    }

    @Test
    void testCourseIDSettersandGetters() {
        int id = 1806188193;
        course.setCourseId(id);
        assertThat(course.getCourseId() == id).isTrue();
    }

    @Test
    void testCourseNameSettersandGetters() {
        String name = "Advanced Programming";
        course.setCourseName(name);
        assertThat(course.getCourseName().equals(name)).isTrue();
    }

    @Test
    void testCourseCalendarSettersandGetters() {
        Calendar calendar = new Calendar();
        course.setCalendar(calendar);
        assertThat(course.getCalendar().getCalendarId() == calendar.getCalendarId()).isTrue();
    }
}

